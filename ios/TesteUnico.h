
#ifdef RCT_NEW_ARCH_ENABLED
#import "RNTesteUnicoSpec.h"

@interface TesteUnico : NSObject <NativeTesteUnicoSpec>
#else
#import "RCTBridgeModule.h"

@interface TesteUnico : NSObject <RCTBridgeModule>ß
#endif

@end
