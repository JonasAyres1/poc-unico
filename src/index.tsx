import React from 'react';
import { NativeModules, Platform, Text } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-teste-unico' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo Go\n';

const TesteUnico = NativeModules.TesteUnico
  ? NativeModules.TesteUnico
  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

    const UnicoConfig = NativeModules.UnicoConfig;

export function multiply(a: number, b: number): Promise<number> {
  return TesteUnico.multiply(a, b);
}

export function Teste(){
  return <Text>{UnicoConfig.getHostInfo()}</Text>;
}
